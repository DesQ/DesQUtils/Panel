/*
 *
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include "TaskBar.hpp"
#include "TaskButton.hpp"

#include <wayqt/WlGlobal.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/Application.hpp>

DesQPanel::TaskBar::TaskBar( QWidget *parent ) : QWidget( parent ) {
    baseLyt = new QHBoxLayout( this );
    baseLyt->setContentsMargins( QMargins() );
    baseLyt->setSpacing( 2 );
    setSizePolicy( QSizePolicy( QSizePolicy::Preferred, QSizePolicy::Preferred ) );

    setLayout( baseLyt );

    setMinimumSize( QSize( 1, panelHeight ) );

    /** Get the current output's id */
    QDBusReply<uint> replyUint = output->call( "QueryActiveOutput" );

    if ( replyUint.isValid() ) {
        mOutputId = replyUint.value();
    }

    /** Get the current workspace */
    QDBusReply<WorkSpace> replyWS = workspaces->call( "QueryActiveWorkspace" );

    if ( replyWS.isValid() ) {
        mCurWS = replyWS.value();
    }

    /** Get the current workspace grid and populate @wsViewsMap */
    QDBusReply<WorkSpaceGrid> replyWSG = workspaces->call( "QueryWorkspaceGrid" );

    if ( replyWSG.isValid() ) {
        mCurWSG = replyWSG.value();
    }

    for ( int r = 0; r < mCurWSG.rows; r++ ) {
        for ( int c = 0; c < mCurWSG.columns; c++ ) {
            WorkSpace             ws{ r, c };
            QDBusReply<QUIntList> replyUList = workspaces->call( "QueryWorkspaceViews", QVariant::fromValue( ws ) );
            printf( "(%d, %d): ", r, c );
            fflush( stdout );
            qDebug() << (replyUList.isValid() ? replyUList.value() : QUIntList() );

            if ( replyUList.isValid() ) {
                wsViewsMap[ ws ] = replyUList.value();
            }

            else {
                wsViewsMap[ ws ] = QUIntList();
            }
        }
    }

    handleWorkspaceChanged( mOutputId, mCurWS );

    setupSignals();
}


void DesQPanel::TaskBar::setupSignals() {
    QDBusConnection::sessionBus().connect(
        "org.DesQ.Wayfire", "/org/DesQ/Wayfire", "wayland.compositor",
        "OutputWorkspaceChanged", "u(ii)", this,
        SLOT( handleWorkspaceChanged( uint, WorkSpace ) )
    );
    QDBusConnection::sessionBus().connect(
        "org.DesQ.Wayfire", "/org/DesQ/Wayfire", "wayland.compositor",
        "ViewAdded", "u", this,
        SLOT( addTask( uint ) )
    );
    // QDBusConnection::sessionBus().connect(
    //     "org.DesQ.Wayfire", "/org/DesQ/Wayfire", "wayland.compositor",
    //     "ViewOutputMoved", "u", this,
    //     SLOT( reloadTasks( uint, WorkSpace ) )
    // );
    QDBusConnection::sessionBus().connect(
        "org.DesQ.Wayfire", "/org/DesQ/Wayfire", "wayland.compositor",
        "ViewWorkspaceChanged", "u(ii)(ii)", this,
        SLOT( handleViewMoved( uint, WorkSpace, WorkSpace ) )
    );

    /** Some view got focus */
    QDBusConnection::sessionBus().connect(
        "org.DesQ.Wayfire", "/org/DesQ/Wayfire", "wayland.compositor",
        "ViewFocusChanged", "u", this,
        SLOT( handleViewFocused( uint ) )
    );

    /** A view was Stickied/Unstickied */
    QDBusConnection::sessionBus().connect(
        "org.DesQ.Wayfire", "/org/DesQ/Wayfire", "wayland.compositor",
        "ViewStickyChanged", "ub", this,
        SLOT( handleViewSticky( uint, bool ) )
    );
}


void DesQPanel::TaskBar::addTaskToPanel( uint view_id ) {
    TaskButton *btn = new TaskButton( view_id );

    connect(
        btn, &TaskButton::closeTask, [ = ]() {
            removeTask( view_id );
        }
    );

    baseLyt->addWidget( btn );
    viewTaskMap[ view_id ] = btn;

    qDebug() << "Adding task to panel" << view_id;
    qDebug() << viewTaskMap;
}


void DesQPanel::TaskBar::addTask( uint view_id ) {
    /**
     * This is the first time this view has come to existence.
     * So we don't have to worry about it existing in the maps.
     */

    reloadTasks();
}


void DesQPanel::TaskBar::removeTask( uint view_id ) {
    reloadTasks();
}


void DesQPanel::TaskBar::handleWorkspaceChanged( uint output_id, WorkSpace ws ) {
    qDebug() << QString( "Changing to workspace (%1, %2) of output %3" ).arg( ws.row ).arg( ws.column ).arg( output_id );
    qDebug() << "Available views:" << wsViewsMap[ ws ];

    if ( output_id != mOutputId ) {
        return;
    }

    /** Update the current workspace */
    mCurWS = ws;

    /** Delete all the existing Taskbuttons */
    for ( uint view_id: viewTaskMap.keys() ) {
        TaskButton *btn = viewTaskMap.take( view_id );

        if ( btn ) {
            btn->close();
            btn->deleteLater();
        }

        else {
            delete btn;
        }
    }

    /** Add new TaskButtons */
    for ( uint view_id: wsViewsMap[ ws ] ) {
        addTaskToPanel( view_id );
    }
}


void DesQPanel::TaskBar::handleViewMoved( uint view_id, WorkSpace from, WorkSpace to ) {
    /** If the view is sticky, do nothing */
    if ( stickyViews.contains( view_id ) ) {
        return;
    }

    /** Remove the view from old workspace */
    wsViewsMap[ from ].removeAll( view_id );

    /** Add it to the new workspace */
    if ( not wsViewsMap[ to ].contains( view_id ) ) {
        wsViewsMap[ to ] << view_id;
    }

    /** If the view was moved from the current workspace */
    if ( mCurWS == from ) {
        wsViewsMap[ from ].removeAll( view_id );

        if ( viewTaskMap.contains( view_id ) ) {
            TaskButton *btn = viewTaskMap.take( view_id );
            btn->close();
            btn->deleteLater();
        }
    }

    /** If the view was moved to the current workspace */
    if ( mCurWS == to ) {
        if ( not wsViewsMap[ to ].contains( view_id ) ) {
            wsViewsMap[ to ] << view_id;
        }

        if ( not viewTaskMap.contains( view_id ) ) {
            addTaskToPanel( view_id );
        }
    }
}


void DesQPanel::TaskBar::handleViewFocused( uint tgtView ) {
    /**
     * The focused view can only be in the current workspace.
     * So we only look at the current workspace views.
     */

    for ( uint view_id: wsViewsMap[ mCurWS ] ) {
        qDebug() << view_id << viewTaskMap.contains( view_id );
        viewTaskMap[ view_id ]->updateFocus( tgtView == view_id );
    }
}


void DesQPanel::TaskBar::handleViewSticky( uint tgtView, bool yes ) {
    /**
     * Just reload the entire layout. It's easy.
     * Otherwise, we need extra dbus calls to check which
     * is the view's main workspace when Unstickied.
     */

    for ( int r = 0; r < mCurWSG.rows; r++ ) {
        for ( int c = 0; c < mCurWSG.columns; c++ ) {
            WorkSpace             ws{ r, c };
            QDBusReply<QUIntList> replyUList = workspaces->call( "QueryWorkspaceViews", QVariant::fromValue( ws ) );
            printf( "(%d, %d): ", r, c );
            fflush( stdout );
            qDebug() << (replyUList.isValid() ? replyUList.value() : QUIntList() );

            if ( replyUList.isValid() ) {
                wsViewsMap[ ws ] = replyUList.value();
            }

            else {
                wsViewsMap[ ws ] = QUIntList();
            }
        }
    }

    /**
     * Now, simply reload the tasks instead of
     * querying if the view exists in the current workspace, etc..
     */
    handleWorkspaceChanged( mOutputId, mCurWS );

    /** Update the button to show sticky */
    if ( viewTaskMap.contains( tgtView ) ) {
        viewTaskMap[ tgtView ]->updateSticky( yes );
    }

    /** Update the stickyViews list */
    if ( yes ) {
        if ( not stickyViews.contains( tgtView ) ) {
            stickyViews << tgtView;
        }
    }

    else {
        if ( stickyViews.contains( tgtView ) ) {
            stickyViews.removeAll( tgtView );
        }
    }
}


void DesQPanel::TaskBar::reloadTasks() {
    /** Get the current workspace */
    QDBusReply<WorkSpace> replyWS = workspaces->call( "QueryActiveWorkspace" );

    if ( replyWS.isValid() ) {
        mCurWS = replyWS.value();
    }

    /** Get the current workspace grid and populate @wsViewsMap */
    QDBusReply<WorkSpaceGrid> replyWSG = workspaces->call( "QueryWorkspaceGrid" );

    if ( replyWSG.isValid() ) {
        mCurWSG = replyWSG.value();
    }

    for ( int r = 0; r < mCurWSG.rows; r++ ) {
        for ( int c = 0; c < mCurWSG.columns; c++ ) {
            WorkSpace             ws{ r, c };
            QDBusReply<QUIntList> replyUList = workspaces->call( "QueryWorkspaceViews", QVariant::fromValue( ws ) );
            printf( "(%d, %d): ", r, c );
            fflush( stdout );
            qDebug() << (replyUList.isValid() ? replyUList.value() : QUIntList() );

            if ( replyUList.isValid() ) {
                wsViewsMap[ ws ] = replyUList.value();
            }

            else {
                wsViewsMap[ ws ] = QUIntList();
            }
        }
    }

    handleWorkspaceChanged( mOutputId, mCurWS );
}
