/*
 *
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#pragma once

#include "Global.hpp"
#include "dbustypes.hpp"

class TaskButton : public QToolButton {
    Q_OBJECT;

    public:
        TaskButton( uint view_id );
        uint taskId();

        void updateFocus( bool yes );
        void updateSticky( bool yes );

    private:
        uint mViewId = 0;

        bool isSticky    = false;
        bool isActive    = false;
        bool isMinimized = false;

    private:
        Q_SLOT void handleViewClosed( uint view_id );
        Q_SLOT void handleAppIdChanged( uint view_id, QString appId );
        Q_SLOT void handleTitleChanged( uint view_id, QString title );
        Q_SLOT void handleAttentionChanged( uint view_id, bool yes );
        Q_SLOT void handleMinimizeChanged( uint view_id, bool yes );

    Q_SIGNALS:
        void closeTask();
};
