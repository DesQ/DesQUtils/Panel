/*
 *
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include "TaskButton.hpp"
#include <desq/Utils.hpp>

#include <filesystem>

static inline QIcon getIconForAppId( QString mAppId ) {
    if ( mAppId.isEmpty() ) {
        return QIcon::fromTheme( "application-x-executable" );
    }

    if ( mAppId == "Unknown" ) {
        return QIcon::fromTheme( "application-x-executable" );
    }

    QStringList appDirs = {
        QDir::home().filePath( ".local/share/applications/" ),
        "/usr/local/share/applications/",
        "/usr/share/applications/",
    };

    QString iconName;
    bool    found = false;
    for ( QString path: appDirs ) {
        if ( DesQ::Utils::exists( path + mAppId + ".desktop" ) ) {
            QSettings desktop( path + mAppId + ".desktop", QSettings::IniFormat );
            iconName = desktop.value( "Desktop Entry/Icon", "application-x-executable" ).toString();

            if ( not iconName.isEmpty() ) {
                found = true;
                break;
            }
        }

        else if ( DesQ::Utils::exists( path + mAppId.toLower() + ".desktop" ) ) {
            QSettings desktop( path + mAppId.toLower() + ".desktop", QSettings::IniFormat );
            iconName = desktop.value( "Desktop Entry/Icon", "application-x-executable" ).toString();

            if ( not iconName.isEmpty() ) {
                found = true;
                break;
            }
        }
    }

    if ( not found ) {
        /* Check all desktop files for */
        for ( QString path: appDirs ) {
            QStringList desktops = QDir( path ).entryList( { "*.desktop" } );
            for ( QString dskf: desktops ) {
                QSettings desktop( path + dskf, QSettings::IniFormat );

                QString exec = desktop.value( "Desktop Entry/Exec", "abcd1234/-" ).toString();
                QString name = desktop.value( "Desktop Entry/Name", "abcd1234/-" ).toString();
                QString cls  = desktop.value( "Desktop Entry/StartupWMClass", "abcd1234/-" ).toString();

                QString execPath( std::filesystem::path( exec.toStdString() ).filename().c_str() );

                if ( mAppId.compare( execPath, Qt::CaseInsensitive ) == 0 ) {
                    iconName = desktop.value( "Desktop Entry/Icon", "application-x-executable" ).toString();
                    break;
                }

                else if ( mAppId.compare( name, Qt::CaseInsensitive ) == 0 ) {
                    iconName = desktop.value( "Desktop Entry/Icon", "application-x-executable" ).toString();
                    break;
                }

                else if ( mAppId.compare( cls, Qt::CaseInsensitive ) == 0 ) {
                    iconName = desktop.value( "Desktop Entry/Icon", "application-x-executable" ).toString();
                    break;
                }
            }
        }
    }

    return QIcon::fromTheme( iconName, QIcon::fromTheme( "application-x-executable" ) );
}


TaskButton::TaskButton( uint view_id ) {
    mViewId = view_id;

    setIconSize( QSize( 22, 22 ) );
    setAutoRaise( true );

    QDBusReply<QString> replyStr = views->call( "QueryViewAppId", mViewId );
    setIcon( getIconForAppId( replyStr.value() ) );

    replyStr = views->call( "QueryViewTitle", mViewId );
    setToolTip( replyStr.value() );

    QDBusReply<bool> replyBool = views->call( "QueryViewActive", mViewId );
    updateFocus( replyBool.value() );

    replyBool = views->call( "QueryViewMinimized", mViewId );
    handleMinimizeChanged( mViewId, replyBool.value() );

    replyBool = views->call( "QueryViewSticky", mViewId );
    updateSticky( replyBool.value() );

    QDBusConnection::sessionBus().connect(
        "org.DesQ.Wayfire", "/org/DesQ/Wayfire", "wayland.compositor",
        "ViewRemoved", "u", this,
        SLOT( handleViewClosed( uint ) )
    );
    QDBusConnection::sessionBus().connect(
        "org.DesQ.Wayfire", "/org/DesQ/Wayfire", "wayland.compositor",
        "ViewAppIdChanged", "us", this,
        SLOT( handleAppIdChanged( uint, QString ) )
    );
    QDBusConnection::sessionBus().connect(
        "org.DesQ.Wayfire", "/org/DesQ/Wayfire", "wayland.compositor",
        "ViewTitleChanged", "us", this,
        SLOT( handleTitleChanged( uint, QString ) )
    );
    QDBusConnection::sessionBus().connect(
        "org.DesQ.Wayfire", "/org/DesQ/Wayfire", "wayland.compositor",
        "ViewAttentionChanged", "ub", this,
        SLOT( handleAttentionChanged( uint, bool ) )
    );
    QDBusConnection::sessionBus().connect(
        "org.DesQ.Wayfire", "/org/DesQ/Wayfire", "wayland.compositor",
        "ViewMinimizedChanged", "ub", this,
        SLOT( handleMinimizeChanged( uint, bool ) )
    );

    /** Minimize, activate or restore the window */
    connect(
        this, &QToolButton::clicked, [ = ]() {
            /** If it's minimized, restore it. Then activate it. */
            QDBusReply<bool> reply = views->call( "QueryViewMinimized", mViewId );

            if ( reply.isValid() and reply.value() ) {
                views->call( "ToggleMinimizeView", mViewId );
                views->call( "FocusView",          mViewId );
                return;
            }

            /** Okay, it's not minimized. If it's activated, then minimize it */
            reply = views->call( "QueryViewActive", mViewId );

            if ( reply.isValid() and reply.value() ) {
                views->call( "ToggleMinimizeView", mViewId );
                return;
            }

            /** Finally, it's neither activated nor minimized. So activate it. */
            else {
                views->call( "FocusView", mViewId );
                return;
            }
        }
    );
}


uint TaskButton::taskId() {
    return mViewId;
}


void TaskButton::updateFocus( bool yes ) {
    isActive = yes;

    /**
     * Sticky can exist in/out of focus, and covers
     * the whole border. So we first set sticky.
     */

    QString ss = "border-radius: 2px; ";

    /** Set the Sticky or clear the border */
    if ( isSticky ) {
        ss += "border: 1px solid purple; ";
    }

    else {
        ss += "border: none; ";
    }

    /** Now set the focus */
    if ( yes ) {
        ss += "border-top: 2px solid palette(Highlight); ";
    }

    else {
        ss += "border: none; ";
    }

    /**
     * Minimized, Active and Sticky can coexist.
     * Active and Sticky are set. Active will
     * not be set when Minimized. So it's safe
     * to query Minimized at this point.
     */
    if ( isMinimized ) {
        ss += "border-bottom: 1px solid orange";
    }

    setStyleSheet( ss );
}


void TaskButton::updateSticky( bool yes ) {
    isSticky = yes;

    QString ss = "border-radius: 2px; ";

    if ( yes ) {
        ss += "border: 1px solid purple; ";
    }

    else {
        ss += "border: none; ";
    }

    /**
     * The view could be Active or Minimized, which are exclusive
     * So we can call them one after the other.
     */

    if ( isActive ) {
        ss += "border-top: 2px solid palette(Highlight);";
    }

    if ( isMinimized ) {
        ss += "border-bottom: 1px solid orange;";
    }

    setStyleSheet( ss );
}


void TaskButton::handleViewClosed( uint view_id ) {
    if ( view_id != mViewId ) {
        return;
    }

    /** Signal the world */
    emit closeTask();

    /** Close the QToolButton -> removes the button from the layout */
    close();

    /** Delete it later */
    deleteLater();
}


void TaskButton::handleAppIdChanged( uint view_id, QString appId ) {
    if ( view_id != mViewId ) {
        return;
    }

    appId += (appId.isEmpty() ? "Unknown" : "");
    setIcon( getIconForAppId( appId ) );
}


void TaskButton::handleTitleChanged( uint view_id, QString title ) {
    if ( view_id != mViewId ) {
        return;
    }

    setToolTip( title );
}


void TaskButton::handleAttentionChanged( uint view_id, bool yes ) {
    if ( view_id != mViewId ) {
        return;
    }

    if ( yes ) {
        setStyleSheet( "background-color: orange;" );
    }

    else {
        QDBusReply<bool> reply = views->call( "QueryViewActive", mViewId );

        if ( reply.isValid() and reply.value() ) {
            setStyleSheet( "border: none; border-top: 2px solid palette(Highlight);" );
        }

        else {
            setStyleSheet( "" );
        }
    }
}


void TaskButton::handleMinimizeChanged( uint view_id, bool yes ) {
    if ( view_id != mViewId ) {
        return;
    }

    isMinimized = yes;

    /**
     * The only other property that can overlap is Sticky.
     * Since Sticky is whole border, and minimized is only
     * the border-bottom, we query Sticky first.
     */

    QString ss = "border-radius: 2px; ";

    if ( isSticky ) {
        ss += "border: 1px solid purple; ";
    }

    else {
        ss += "border: none; ";
    }

    if ( yes ) {
        ss += "border-bottom: 1px solid orange; ";
    }

    else {
        ss += "border: none; ";
    }

    setStyleSheet( ss );
}
