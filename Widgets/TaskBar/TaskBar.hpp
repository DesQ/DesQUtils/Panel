/*
 *
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#pragma once

#include "Global.hpp"
#include "dbustypes.hpp"

namespace DesQPanel {
    class TaskBar;
}

class TaskButton;

class DesQPanel::TaskBar : public QWidget {
    Q_OBJECT;

    public:
        TaskBar( QWidget *parent = NULL );

    private:
        QHBoxLayout *baseLyt;

        /** The workspace-views map for the current output */
        QMap<WorkSpace, QUIntList> wsViewsMap;
        QMap<uint, TaskButton *> viewTaskMap;
        QUIntList stickyViews;

        WorkSpaceGrid grid;

        uint mOutputId = 1;
        WorkSpace mCurWS;
        WorkSpaceGrid mCurWSG;

        void setupSignals();
        void addTaskToPanel( uint );

        Q_SLOT void addTask( uint );
        Q_SLOT void removeTask( uint );

        Q_SLOT void handleWorkspaceChanged( uint, WorkSpace );
        Q_SLOT void handleViewMoved( uint, WorkSpace, WorkSpace );

        Q_SLOT void handleViewFocused( uint );
        Q_SLOT void handleViewSticky( uint, bool );

        Q_SLOT void reloadTasks();
};
