/*
 *
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project ( https://gitlab.com/desq/ )
 * Qt5 example of analog clock, and Lenovo K8 Note's clock widget
 * are the inspiration for this. Some parts of the code are taken
 * from Qt5's example
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include "Global.hpp"
#include "PagerWidget.hpp"

WorkspaceWidget::WorkspaceWidget( WorkSpace ws, QWidget *parent ) : QWidget( parent ) {
    mCurWS = ws;
    setFixedSize( QSize( 32, 18 ) );
}


WorkSpace WorkspaceWidget::workSpace() {
    return mCurWS;
}


void WorkspaceWidget::highlight( bool yes ) {
    mHighlight = yes;
    repaint();
}


void WorkspaceWidget::mousePressEvent( QMouseEvent *mEvent ) {
    if ( mEvent->button() == Qt::LeftButton ) {
        mPressed = true;
    }

    mEvent->accept();
}


void WorkspaceWidget::mouseReleaseEvent( QMouseEvent *mEvent ) {
    if ( (mEvent->button() == Qt::LeftButton) and mPressed ) {
        emit switchWorkSpace( mCurWS );
    }

    mEvent->accept();
}


void WorkspaceWidget::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( this );

    painter.setRenderHint( QPainter::Antialiasing );

    if ( mHighlight ) {
        QColor clr( palette().color( QPalette::Highlight ) );
        painter.setPen( clr );
        clr.setAlphaF( 0.5 );
        painter.setBrush( clr );
    }

    else {
        QColor clr( palette().color( QPalette::Window ) );
        painter.setPen( clr );
        clr.setAlphaF( 0.5 );
        painter.setBrush( clr );
    }

    painter.drawRoundedRect( rect().adjusted( 1, 1, -1, -1 ), 2, 2 );

    painter.end();

    pEvent->accept();
}


PagerWidget::PagerWidget( QWidget *parent ) : QWidget( parent ) {
    qRegisterMetaType<QUIntList>( "QUIntList" );
    qDBusRegisterMetaType<QUIntList>();

    qRegisterMetaType<WorkSpace>( "WorkSpace" );
    qDBusRegisterMetaType<WorkSpace>();

    qRegisterMetaType<WorkSpaces>( "WorkSpaces" );
    qDBusRegisterMetaType<WorkSpaces>();

    qRegisterMetaType<WorkSpaceGrid>( "WorkSpaceGrid" );
    qDBusRegisterMetaType<WorkSpaceGrid>();

    QDBusConnection::sessionBus().connect(
        "org.DesQ.Wayfire",
        "/org/DesQ/Wayfire",
        "wayland.compositor",
        "OutputWorkspaceChanged",
        "u(ii)",
        this,
        SLOT( highlightWorkspace( uint, WorkSpace ) )
    );

    QDBusConnection::sessionBus().connect(
        "org.DesQ.Wayfire",
        "/org/DesQ/Wayfire",
        "wayland.compositor",
        "OutputChanged",
        "u",
        this,
        SLOT( populateLayout( uint ) )
    );

    wsLyt = new QGridLayout();
    wsLyt->setContentsMargins( QMargins() );
    setLayout( wsLyt );

    QDBusReply<uint> opReply = output->call( "QueryActiveOutput" );

    if ( opReply.isValid() ) {
        populateLayout( opReply.value() );
    }
}


void PagerWidget::populateLayout( uint output_id ) {
    mCurOutput = output_id;

    /* Delete the previously added WorkSpaceWidgets */
    while ( wsList.count() ) {
        WorkspaceWidget *w = wsList.takeAt( 0 );
        w->close();
        w->deleteLater();
    }

    QDBusReply<WorkSpaceGrid> wsGrid = output->call( "QueryOutputWorkspaceGrid", output_id );

    if ( wsGrid.isValid() ) {
        WorkSpaceGrid wsg = wsGrid.value();

        for ( int r = 0; r < wsg.rows; r++ ) {
            for ( int c = 0; c < wsg.columns; c++ ) {
                WorkspaceWidget *wsw = new WorkspaceWidget( { r, c }, this );
                connect(
                    wsw, &WorkspaceWidget::switchWorkSpace, [ = ]( WorkSpace ws ) {
                        if ( mCurWS == ws ) {
                            output->call( "ShowDesktop" );
                        }

                        else {
                            output->call( "ChangeWorkspace", mCurOutput, QVariant::fromValue( ws ) );
                        }
                    }
                );
                wsLyt->addWidget( wsw, r, c );
                wsList << wsw;
            }
        }
    }

    QDBusReply<WorkSpace> wsx = output->call( "QueryOutputWorkspace", mCurOutput );

    if ( wsx.isValid() ) {
        highlightWorkspace( mCurOutput, wsx.value() );
    }
}


void PagerWidget::highlightWorkspace( uint output_id, WorkSpace ws ) {
    if ( output_id != mCurOutput ) {
        return;
    }

    mCurWS = ws;

    for ( WorkspaceWidget *wsw: wsList ) {
        if ( wsw->workSpace() == ws ) {
            wsw->highlight( true );
        }

        else {
            wsw->highlight( false );
        }
    }
}
