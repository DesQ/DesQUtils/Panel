/*
 *
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <QtWidgets>

#include "Clock.hpp"
#include "StyleSheet.hpp"

ClockLabel::ClockLabel( QWidget *parent ) : QLabel( parent ) {
    setText( QDateTime::currentDateTime().toString( "  ddd dd, hh:mm:ss AP  " ) );
    setAlignment( Qt::AlignCenter );

    setFont( QFont( font().family(), 10, QFont::Bold ) );
    setAlignment( Qt::AlignCenter );
    setSizePolicy( QSizePolicy( QSizePolicy::Preferred, QSizePolicy::Preferred ) );

    fm = new QFontMetrics( font() );

    cTimer = new QBasicTimer();
    cTimer->start( 1000, this );

    setMouseTracking( true );
}


void ClockLabel::enterEvent( QEvent *event ) {
    setStyleSheet( "background-color: rgba(90, 90, 90, 60);" );
    QLabel::enterEvent( event );
}


void ClockLabel::leaveEvent( QEvent *event ) {
    setStyleSheet( "" );
    QLabel::leaveEvent( event );
}


void ClockLabel::mousePressEvent( QMouseEvent *mEvent ) {
    if ( mEvent->button() == Qt::LeftButton ) {
        emit pressed();
    }

    mEvent->accept();
}


void ClockLabel::mouseReleaseEvent( QMouseEvent *mEvent ) {
    if ( rect().contains( mEvent->pos() ) and (mEvent->button() == Qt::LeftButton) ) {
        emit clicked();
        emit released();
    }

    mEvent->accept();
}


void ClockLabel::timerEvent( QTimerEvent *tEvent ) {
    if ( tEvent->timerId() == cTimer->timerId() ) {
        QString dt = QDateTime::currentDateTime().toString( "  ddd dd, hh:mm:ss AP  " );
        setText( dt );

        setMinimumWidth( std::max( fm->horizontalAdvance( dt ), minimumWidth() ) );
    }
}
