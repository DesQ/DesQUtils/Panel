/*
 *
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#pragma once

#include <QtWidgets>

class ClockLabel : public QLabel {
    Q_OBJECT

    public:
        ClockLabel( QWidget *parent = NULL );

    private:
        QBasicTimer *cTimer;
        QFontMetrics *fm;

    protected:
        void enterEvent( QEvent * ) override;
        void leaveEvent( QEvent * ) override;

        void mousePressEvent( QMouseEvent * ) override;
        void mouseReleaseEvent( QMouseEvent * ) override;

        void timerEvent( QTimerEvent * ) override;

    signals:
        void pressed();
        void clicked();
        void released();
};
