/*
 *
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <QtWidgets>

#include "ShowDesktop.hpp"

ShowDesktop::ShowDesktop( QWidget *parent ) : QWidget( parent ) {
    setFixedSize( 10, panelHeight - 2 );

    QWidget *base = new QWidget( this );
    base->setObjectName( "base" );
    base->setFixedSize( 10, panelHeight - 2 );

    setSizePolicy( QSizePolicy( QSizePolicy::Preferred, QSizePolicy::MinimumExpanding ) );
    setStyleSheet( "#base{ border: 1px solid rgba(255, 255, 255, 90); }" );

    setMouseTracking( true );
}


void ShowDesktop::enterEvent( QEvent *event ) {
    setStyleSheet( "#base{ border: 1px solid rgba(255, 255, 255, 90); background-color: rgba(90, 90, 90, 60); }" );
    QWidget::enterEvent( event );

    emit showDesktop();
}


void ShowDesktop::leaveEvent( QEvent *event ) {
    setStyleSheet( "#base{ border: 1px solid rgba(255, 255, 255, 90); }" );
    QWidget::leaveEvent( event );

    emit hideDesktop();
}


void ShowDesktop::mousePressEvent( QMouseEvent *mEvent ) {
    if ( mEvent->button() == Qt::LeftButton ) {
        pressed = true;
    }

    mEvent->accept();
}


void ShowDesktop::mouseReleaseEvent( QMouseEvent *mEvent ) {
    if ( pressed ) {
        if ( rect().contains( mEvent->pos() ) and (mEvent->button() == Qt::LeftButton) ) {
            emit clicked();
        }
    }

    pressed = false;

    mEvent->accept();
}
