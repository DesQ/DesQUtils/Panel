/*
 *
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project ( https://gitlab.com/desq/ )
 * Qt5 example of analog clock, and Lenovo K8 Note's clock widget
 * are the inspiration for this. Some parts of the code are taken
 * from Qt5's example
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <QtWidgets>

#include "Global.hpp"
#include "ClassicPanel.hpp"
#include "StyleSheet.hpp"

#include "Widgets.hpp"
#include "Clock.hpp"
#include "TaskBar.hpp"
#include "PagerWidget.hpp"
#include "ShowDesktop.hpp"

#include <desq/Utils.hpp>

#include <desq/desq-config.h>
#include <desqui/ShellPlugin.hpp>

static inline QWidget * getSNI() {
    QWidget *sni = new QWidget();

    /* SNI */
    QPluginLoader loader( PluginPath "shell/libdesq-plugin-sni.so" );
    QObject       *pObject = loader.instance();

    if ( pObject ) {
        DesQ::Plugin::Shell *plugin = qobject_cast<DesQ::Plugin::Shell *>( pObject );

        if ( !plugin ) {
            qWarning() << "Plugin Error:" << loader.errorString();
            qWarning() << "SNI will not be available";
        }

        else {
            sni = plugin->widget( nullptr );
            sni->setFixedHeight( panelHeight - 2 );
            sni->setStyleSheet( ToolButtonStyle );
        }
    }

    else {
        qWarning() << "Plugin Error:" << loader.errorString();
        qWarning() << "SNI will not be available";
    }

    return sni;
}


DesQPanel::ClassicUI::ClassicUI() : DesQPanel::AbstractUI() {
    /* No frame, and stay at bottom */
    Qt::WindowFlags wFlags = Qt::FramelessWindowHint | Qt::WindowStaysOnBottomHint | Qt::BypassWindowManagerHint;

    setAttribute( Qt::WA_TranslucentBackground );

    /* Set the windowFlags */
    setWindowFlags( wFlags );

    setWindowTitle( tr( "DesQ Panel" ) );

    buildPanel();
    autoResize();

    connect( qApp->primaryScreen(), &QScreen::geometryChanged, this, &DesQPanel::ClassicUI::autoResize );
}


void DesQPanel::ClassicUI::autoResize() {
    // Span the entire width of the screen
    QSize scrSize = qApp->primaryScreen()->size();

    setFixedSize( scrSize.width(), panelHeight );

    // Move to the top left of the screen - useless in wayland :p
    move( 0, 0 );
}


void DesQPanel::ClassicUI::buildPanel() {
    menuButton = new QToolButton();
    menuButton->setIcon( QIcon::fromTheme( "desq" ) );
    menuButton->setAutoRaise( true );
    menuButton->setIconSize( QSize( panelHeight - 2, panelHeight - 2 ) );

    connect( menuButton, &QToolButton::clicked, [ = ]() {
                 QProcess::startDetached( DesQ::Utils::getUtilityPath( "menu" ), {} );
             } );

    pager = new PagerWidget( this );
    clock = new ClockLabel( this );
    tasks = new DesQPanel::TaskBar( this );

    connect( clock, &ClockLabel::clicked, [ = ]() {
                 QProcess::startDetached( "coretime", {} );
             } );

    tray       = getSNI();
    desktopBtn = new ShowDesktop();

    connect( desktopBtn, &ShowDesktop::showDesktop, [ = ]() {
                 output->call( "ShowDesktop" );
             } );
    connect( desktopBtn, &ShowDesktop::hideDesktop, [ = ]() {
                 output->call( "ShowDesktop" );
             } );

    widgetLyt = new QHBoxLayout();
    widgetLyt->setSpacing( 0 );
    widgetLyt->setContentsMargins( QMargins() );

    widgetLyt->addWidget( menuButton );
    widgetLyt->addWidget( new QLabel( " | " ) );
    widgetLyt->addWidget( pager );
    widgetLyt->addWidget( new QLabel( " | " ) );
    widgetLyt->addWidget( tasks );
    widgetLyt->addStretch();
    widgetLyt->addWidget( tray );
    widgetLyt->addWidget( clock );
    widgetLyt->addWidget( desktopBtn );

    QWidget *base = new QWidget( this );
    base->setLayout( widgetLyt );

    QHBoxLayout *baseLyt = new QHBoxLayout();
    baseLyt->setContentsMargins( QMargins( 1, 1, 1, 1 ) );
    baseLyt->addWidget( base );

    setLayout( baseLyt );
}


void DesQPanel::ClassicUI::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( this );

    painter.setRenderHint( QPainter::Antialiasing );

    painter.save();
    painter.setBrush( QColor( 0, 0, 0, 230 ) );
    painter.setPen( Qt::NoPen );
    painter.drawRect( 0, 0, width(), height() );
    painter.restore();

    painter.end();

    QWidget::paintEvent( pEvent );
}
