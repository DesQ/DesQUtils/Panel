/*
 *
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <QtWidgets>

#include "Widgets.hpp"
#include "StyleSheet.hpp"

AppButton::AppButton( QWidget *parent ) : QWidget( parent ) {
    appBtn = new QPushButton( QIcon::fromTheme( "desq" ), "DesQ" );
    appBtn->setStyleSheet( PushButtonStyle );
    appBtn->setMinimumWidth( 90 );
    appBtn->setFlat( true );

    appListBtn = new QToolButton();
    appListBtn->setStyleSheet( ToolButtonStyle );
    appListBtn->setToolButtonStyle( Qt::ToolButtonIconOnly );
    appListBtn->setIcon( QIcon( ":/icons/window-list.png" ) );
    appListBtn->setAutoRaise( true );
    updateApps();

    QHBoxLayout *lyt = new QHBoxLayout();
    lyt->setSpacing( 0 );
    lyt->setContentsMargins( QMargins() );
    lyt->addWidget( appBtn );
    lyt->addWidget( appListBtn );

    setLayout( lyt );

    connect(
        appListBtn, &QToolButton::clicked, [ = ]() {
            appList->exec( QPoint( 90, appBtn->height() ) );
        }
    );
}


void AppButton::setFixedHeight( int height ) {
    QWidget::setFixedHeight( height );
    appBtn->setFixedHeight( height );
    appListBtn->setFixedSize( height, height );
}


void AppButton::updateApps() {
    appList = new QMenu( this );

    // QAction *addAction(const QIcon &icon, const QString &text, const QObject *receiver, const char
    // *member, const QKeySequence &shortcut = 0)
    appList->addAction(
        QIcon::fromTheme( "atom" ), "Atom", this, [ = ]() {
            // Code To switch to this app
        }
    );

    appList->addAction(
        QIcon::fromTheme( "desq-term" ), "DesQ Term", this, [ = ]() {
            // Code To switch to this app
        }
    );

    appList->addAction(
        QIcon::fromTheme( "desq-files" ), "DesQ Files", this, [ = ]() {
            // Code To switch to this app
        }
    );

    appList->addAction(
        QIcon::fromTheme( "firefox" ), "Firefox", this, [ = ]() {
            // Code To switch to this app
        }
    );

    if ( appList->actions().count() ) {
        setEnabled( true );
    }

    else {
        setDisabled( true );
    }
}


TrayButton::TrayButton( QWidget *parent ) : QToolButton( parent ) {
    setIcon( QIcon::fromTheme( ":/icons/tray.png" ) );
    setStyleSheet( ToolButtonStyle );

    setToolTip( "Click to show status notifier icons." );
    setDisabled( false );
}


InfoButton::InfoButton( QWidget *parent ) : QToolButton( parent ) {
    setIcon( QIcon::fromTheme( ":/icons/info.png" ) );
    setStyleSheet( ToolButtonStyle );

    setToolTip( "Click to show received notifications" );
}


PowerButton::PowerButton( QWidget *parent ) : QToolButton( parent ) {
    setIcon( QIcon::fromTheme( ":/icons/power.png" ) );
    setStyleSheet( ToolButtonStyle );

    setToolTip( "Click to open the logout dialog" );
}


QWidget * Spacer::horizontal( int size ) {
    QWidget *spacer = new QWidget();

    spacer->setFixedWidth( size );
    spacer->setMinimumHeight( 1 );
    spacer->setStyleSheet( "background-color: transparent;" );

    return spacer;
}


QWidget * Spacer::vertical( int size ) {
    QWidget *spacer = new QWidget();

    spacer->setMinimumWidth( 1 );
    spacer->setFixedHeight( size );
    spacer->setStyleSheet( "background-color: transparent;" );

    return spacer;
}
