# DesQ Panel
# A simple panel for DesQ Shell, inspired by GNOME and KDE's panels.

Currently, a menu button, pager, window list, system tray, clock and desktop quick-peek.
Once mature, all the widgets will be customizable. It will even be possible to have menubar
in thee panel like gnome.

### Notes for compiling (Qt5) - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/DesQ/DesQUtils/Panel.git DesQPanel`
- Enter the `DesQPanel` folder
  * `cd DesQPanel`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### Dependencies:
* Qt5 (qtbase5-dev, qtbase5-dev-tools)
* wayland (libwayland-dev, wayland-protocols)
* wayfire (Compiled from git, runtime)
* dbusqt wayfire plugin (https://gitlab.com/wayfireplugins/dbusqt.git)
* libdesq (https://gitlab.com/DesQ/libdesq.git)
* wlrootsqt (https://gitlab.com/desktop-frameworks/wayqt.git)


### Known Bugs
* Please test and let me know

### Upcoming
* Any other feature you request for... :)
