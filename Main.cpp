/*
 *
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

// Local Headers
#include "Global.hpp"
#include "AbstractPanel.hpp"
#include "ClassicPanel.hpp"

#include <desq/Utils.hpp>
#include <desq/desq-config.h>

#include <wayqt/WlGlobal.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/LayerShell.hpp>
#include <wayqt/Application.hpp>

DFL::Settings *shellSett;
DFL::Settings *panelSett;
int            panelHeight;

QDBusInterface *compositor;
QDBusInterface *workspaces;
QDBusInterface *output;
QDBusInterface *views;

int main( int argc, char *argv[] ) {
#if (QT_VERSION >= QT_VERSION_CHECK( 5, 6, 0 ) )
        QApplication::setAttribute( Qt::AA_EnableHighDpiScaling );
#endif

    DesQ::log = fopen( "/tmp/desq-panel-1000.log", "w" );
#ifdef LOGGING
        DesQ::SHOW_INFO_ON_CONSOLE     = (LOGGING <= 1);
        DesQ::SHOW_DEBUG_ON_CONSOLE    = (LOGGING <= 2);
        DesQ::SHOW_WARNING_ON_CONSOLE  = (LOGGING <= 3);
        DesQ::SHOW_CRITICAL_ON_CONSOLE = (LOGGING <= 4);
        DesQ::SHOW_FATAL_ON_CONSOLE    = (LOGGING <= 5);
#endif

    qInstallMessageHandler( DesQ::Logger );

    qputenv( "QT_WAYLAND_USE_BYPASSWINDOWMANAGERHINT", QByteArrayLiteral( "1" ) );
    WQt::Application *app = new WQt::Application( "Panel", argc, argv );

    if ( app->isRunning() ) {
        qDebug() << "Panel is already running";
        return 0;
    }

    /** Prepare the DesQ Settings */
    shellSett = new DFL::Settings( "DesQ", "Shell", ConfigPath );
    panelSett = new DFL::Settings( "DesQ", "Panel", ConfigPath );

    panelHeight = panelSett->value( "Height" );

    /** Prepare the DBus Interfaces */
    output     = new QDBusInterface( "org.DesQ.Wayfire", "/org/DesQ/Wayfire", "wayland.compositor.output", QDBusConnection::sessionBus() );
    compositor = new QDBusInterface( "org.DesQ.Wayfire", "/org/DesQ/Wayfire", "wayland.compositor", QDBusConnection::sessionBus() );
    workspaces = new QDBusInterface( "org.DesQ.Wayfire", "/org/DesQ/Wayfire", "wayland.compositor.workspace", QDBusConnection::sessionBus() );
    views      = new QDBusInterface( "org.DesQ.Wayfire", "/org/DesQ/Wayfire", "wayland.compositor.views", QDBusConnection::sessionBus() );

    DesQPanel::AbstractUI *panel;

    if ( (QString)panelSett->value( "UI" ) == "Classic" ) {
        panel = new DesQPanel::ClassicUI();
    }

    /* Until we build the Modern UI */
    else {
        panel = new DesQPanel::ClassicUI();
    }

    panel->setWindowFlags( panel->windowFlags() | Qt::BypassWindowManagerHint );
    panel->show();

    /** This is exclusively for wayfire - requires wayfire dbusqt plugin */
    if ( WQt::isWayfire() ) {
        WQt::Registry *registry = qApp->waylandRegistry();

        WQt::LayerShell::LayerType lyr = WQt::LayerShell::Bottom;
        switch ( (int)panelSett->value( "AutoHide" ) ) {
            case DesQPanel::AbstractUI::Never: {
                lyr = WQt::LayerShell::Top;
                break;
            }

            case DesQPanel::AbstractUI::Always: {
                lyr = WQt::LayerShell::Bottom;
                break;
            }

            case DesQPanel::AbstractUI::WindowsCanCover: {
                lyr = WQt::LayerShell::Bottom;
                break;
            }
        }

        WQt::LayerSurface *cls = registry->layerShell()->getLayerSurface( panel->windowHandle(), nullptr, lyr, "panel" );

        /** Stuck to the Top/Bottom (for now). We need to implement settings */
        switch ( (int)panelSett->value( "Position" ) ) {
            case DesQPanel::AbstractUI::Left: {
                cls->setAnchors( WQt::LayerSurface::Bottom );
                break;
            }

            case DesQPanel::AbstractUI::Top: {
                cls->setAnchors( WQt::LayerSurface::Top );
                break;
            }

            case DesQPanel::AbstractUI::Right: {
                cls->setAnchors( WQt::LayerSurface::Bottom );
                break;
            }

            case DesQPanel::AbstractUI::Bottom: {
                cls->setAnchors( WQt::LayerSurface::Bottom );
                break;
            }
        }

        /** Size of our surface */
        cls->setSurfaceSize( panel->size() );

        /** No margins (for now). We can implment DesQ Dock like settings for floating panel that hides */
        cls->setMargins( QMargins( 0, 0, 0, 0 ) );

        /** Do not move this surface at any cost. */
        cls->setExclusiveZone( panelHeight );

        /** We may need keyboard interaction (in future). Nothing needed now */
        cls->setKeyboardInteractivity( WQt::LayerSurface::NoFocus );

        /** Commit to our choices */
        cls->apply();
    }

    return app->exec();
}
